#Using Python
import os

#Print numbers from 1 to 100 and writes them into file
def writeNumbersToFile(index):
  count = 1
  result = []
  if os.path.exists("numbers.txt"):
    os.remove("numbers.txt")
  try:
    if index > 0:
      f = open("numbers.txt", "w")
      while count <= index:
        result.append(str(count)+ ' ')
        f.write(str(count)+ ' ')
        print(count)
        count += 1
      f.close()
    else:
      print("Not a valid number, try again")
  except ValueError:
    print("Not a valid number, try again")

  return result
  

#Read and print to console half of the numbers from the file
def readNumbersFromFile():
  counter = 0
  try: 
    if os.path.exists("numbers.txt"):
      halfCount = len(fileReader())/2
      numberContent = fileReader()
      while counter < halfCount:
        print(numberContent[counter], end = " ")
        counter += 1
      print('\n')
    else:
      print("There is not such file to read value from, try again")
  except ValueError:
      print("There is not such file to read value from, try again")


#Abstraction of method readNumbersFromFile for  testable function 
def fileReader():
  f = open("numbers.txt", "r")
  numberContent = f.read().split()
  f.close()
  return numberContent

  

writeNumbersToFile(100) 
readNumbersFromFile()

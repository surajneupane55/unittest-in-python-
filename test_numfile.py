import unittest
import number
import os


class TestNumFile(unittest.TestCase):

    def test_writeNumbersToFile(self):
        self.assertEqual(len(number.writeNumbersToFile(100)), 100)
        self.assertLess(len(number.writeNumbersToFile(10)), 100)
        self.assertGreater(len(number.writeNumbersToFile(100)), 10)
        self.assertEqual((number.writeNumbersToFile(0)), [] )
        self.assertEqual(os.path.exists('numbers.txt'), False)
        self.assertEqual(len(number.writeNumbersToFile(100)), 100)
        self.assertEqual(os.path.exists('numbers.txt'), True)
    
    def test_readNumberFromFile(self):
        self.assertEqual(len(number.writeNumbersToFile(50)), 50)
        self.assertEqual(len(number.fileReader()), 50)
        self.assertNotEqual(len(number.fileReader()), 100)
        self.assertLessEqual(len(number.fileReader()), 50)
        
        
        
if __name__ == '__main__':
    unittest.main()  